const path = require('path');
const ExtractTextPlugin = require("extract-text-webpack-plugin");

const config = {
  entry: [
    "./src/js/index.js",
    "./src/scss/main.scss",
    "./src/index.html"
  ],
  output: {
    filename: "./js/bundle.js"
    // Можна вказати свою папку:
    // path: path.resolve(__dirname, 'xyu'),
    // filename: "js/bundle.js"
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        include: path.resolve(__dirname, 'src/js'),
        use: {
          loader: 'babel-loader',
          options: {
            presets: 'env'
          }
        }
      },
      {
        test: /\.(png|jpg)$/,
        use: [
          {
            loader: 'file-loader',
            options: {name: 'img/[name].[ext]'}
          }
        ]
      },
      {
        test: /\.(sass|scss)$/,
        include: path.resolve(__dirname, 'src/scss/main.scss'),
        use: ExtractTextPlugin.extract({
          use: [{
            loader: "css-loader",
            options: {
              sourceMap: true
            }
          },
            {
              loader: "resolve-url-loader"
            },
            {
              loader: "sass-loader",
              options: {
                sourceMap: true
              }
            }
          ]
        })
      },
      {
        test: /\.html/,
        loader: 'file-loader?name=[name].[ext]',
      }
    ]
  },
  plugins: [
    // Плагін для CSS, вказує точку виходу
    new ExtractTextPlugin({
      filename: './style.css',
      allChunks: true,
    }),
  ],
  devtool: "source-map",
  devServer: {
    overlay: true
  }
};

module.exports = config;